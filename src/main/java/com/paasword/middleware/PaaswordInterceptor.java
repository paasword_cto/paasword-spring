package com.paasword.middleware;

import java.time.Instant;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

@Component
public class PaaswordInterceptor implements HandlerInterceptor {

	@Override
	   public boolean preHandle
	      (HttpServletRequest request, HttpServletResponse response, Object handler) 
	      throws Exception {
	      
		  String token = request.getHeader("x-auth-token");
		  if (token == null || token.trim().isEmpty()) {
			  String json = "{\"ErrorType\": \"MISSING_ELEMENT\", \"ErrorMessage\": \"x-auth-token\"}";
			  response.setContentType("application/json");
			  response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			  response.getWriter().write(json);
			  return false;
		  }
		  
		  String appPrivateKey = System.getenv("PAASWORD_APP_PRIVATE_KEY");
		  if (appPrivateKey == null || appPrivateKey.trim().isEmpty()) {
			  String json = "{\"ErrorType\": \"MISSING_ELEMENT\", \"ErrorMessage\": \"PAASWORD_APP_PRIVATE_KEY\"}";
			  response.setContentType("application/json");
			  response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			  response.getWriter().write(json);
			  return false;
		  }
		  
		  Jwt jwtToken = JwtHelper.decode(token);
		  String claims = jwtToken.getClaims();
		  HashMap user = new ObjectMapper().readValue(claims, HashMap.class);
		  if (user.get("iat") != null && user.get("AutoLogout") != null) {
			  long loginTime = Long.parseLong(user.get("iat").toString()) * 1000;
			  long now = Instant.now().toEpochMilli();
			  int hoursSinceLogin = Math.round((now - loginTime)/3600000);
			  int hoursAllowed = (int)((HashMap)user.get("AutoLogout")).get("LogoutEveryXHours");
			  if (hoursSinceLogin > hoursAllowed) {
				  String json = "{\"ErrorType\": \"SESSION_EXPIRED\", \"ErrorMessage\": \"\"}";
				  response.setContentType("application/json");
				  response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				  response.getWriter().write(json);
				  return false;
			  }
		  }
		  request.setAttribute("user", user);
		  
	      return true;
	   }

}
