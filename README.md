# Paas-Word Java Spring Middleware

[Paas-Word](https://www.paas-word.com) is an online authentication and user management service.
This Java Spring middleware library by [Paas-Word](https://www.paas-word.com) enables website owners with a Java Spring backend to restrict their endpoints to authenticated users only and retrieve user data. 

## Usage

1. Create a free account at [Paas-Word](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your website with a token, send this token to your backend in the "x-auth-token" header.

## Installation

Add the following to your pom.xml file

```java
<dependency>
	<groupId>com.paas-word.middleware</groupId>
	<artifactId>paasword-authenticate</artifactId>
	<version>1.0.0</version>
</dependency>
```
## Set Private Key as Environment Variable
Create an app on [Paas-Word](https://www.paas-word.com) and then set its Private Key as an environment variable. 

For example, on Eclipse, right click project -> Run Configurations ->Environment :

```java
Variable: PAASWORD_APP_PRIVATE_KEY
Value: 93f56f52-957d-4953-93a6-c5492e79778b
```

## SpringBootApplication 
For SpringBootApplication, add the following annotation.

```java
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.paasword.middleware","com.example.website"})
@SpringBootApplication
public class MyApplication {
...
```

## Guard all endpoints
Create a new file InterceptorAppConfig.java

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.paasword.middleware.PaaswordInterceptor;

@Component
public class InterceptorAppConfig implements WebMvcConfigurer {
   @Autowired
   PaaswordInterceptor paaswordInterceptor;

   @Override
   public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(paaswordInterceptor);
   }
}

```

## Guard specific routes against unauthenticated users
Add the routes dedicated to logged-in users. 

```java
@Override
public void addInterceptors(InterceptorRegistry registry) {
   registry.addInterceptor(paaswordInterceptor).addPathPatterns("/employees/{id}");
}

```

## Retrieve user information

```java
@GetMapping("/employees/{id}")
Employee one(@RequestAttribute("user")HashMap user, @PathVariable Long id) {
	System.out.println(user);
	return repository.findById(id);
}
```

